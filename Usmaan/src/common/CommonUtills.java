/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 *
 * @author User
 */
public class CommonUtills {
   public static DecimalFormat decimalFormat = new DecimalFormat("###,###,###,##0.00");
    public static BigDecimal HUNDRED = new BigDecimal(100);
    
    public static void DoubleOnly(java.awt.event.KeyEvent evt,JTextField field) {
        char c = evt.getKeyChar();
        if (field.getText().contains(".")) {
            if ((c == KeyEvent.VK_PERIOD)) {

               
                evt.consume();
            }

        }

        if (!((c >= '0') && (c <= '9')
                || (c == KeyEvent.VK_BACK_SPACE)
                || (c == KeyEvent.VK_DELETE) || (c == KeyEvent.VK_PERIOD))) {

            
            evt.consume();
        }
    }

}
