/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import java.sql.Connection;
import java.sql.DriverManager;

public class JDBC {

    static Connection connection=null;

    public static Connection usmaanEnterprises_con() throws Exception {
        if(connection==null){
        
        String url;
        url = "jdbc:mysql://" + "localhost:3306" + "/usmaanEnterprises?zeroDateTimeBehavior=convertToNull&noAccessToProcedureBodies=true&autoReconnect=true&useUnicode=yes&characterEncoding=UTF-8";
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        connection = DriverManager.getConnection(url, "root", "123");
        
        }
       return connection;
    }
}
