/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;
 
import Models.Brands;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;

/**
 *
 * @author User
 */
public class utill {

    public static ArrayList<Brands> getBrands(Connection connection) throws Exception {
        PreparedStatement ps = connection.prepareStatement("SELECT brandid, brandname FROM `brands` order by brandid ");
        ResultSet rs = ps.executeQuery();
        ArrayList<Brands> brands = new ArrayList<Brands>();
        if (rs.isBeforeFirst()) {            
            while (rs.next()) {               
                Brands brand = new Brands();
                brand.setBrandid(rs.getInt("brandid"));
                brand.setBrandname(rs.getString("brandname"));
                brands.add(brand);
            }
        }
        return brands;
    }
    public static ArrayList getUOM(Connection connection) throws Exception {
        PreparedStatement ps = connection.prepareStatement("SELECT uom FROM `uom` order by uom");
        ResultSet rs = ps.executeQuery();
        ArrayList uom = new ArrayList();
        if (rs.isBeforeFirst()) {            
            while (rs.next()) {               
            
                uom.add(rs.getString("uom"));
            }
        }
        return uom;
    }

}
