/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usmaan;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
/**
 *
 * @author User
 */
public class AddTab extends JPanel implements ActionListener  {
    

    private JLabel title = new JLabel();
    JButton closeButton = new JButton();
    int tabIndex;
    private JTabbedPane tabbedPane = null;

    public void setTitle(String title, JTabbedPane tabbedPane) {
        this.title.setText(title);
        this.tabbedPane = tabbedPane;
        this.setFont(new Font("Tahoma", 1, 11));
    }

    public void updateTitle(String title) {
        this.title.setText(title);
    }

    public void setTabbedPane(JTabbedPane tabbedPane) {
        this.tabbedPane = tabbedPane;
    }

    public void setCloseAction(ActionListener al) {
    }

    public void setTabIndex(int index) {
        this.tabIndex = index;
    }

    public void init() {
        closeButton.setSize(10, 10);
        closeButton.setBorder(new EmptyBorder(0, 0, 0, 0));
        closeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/cancel-icon.png")));
        closeButton.setContentAreaFilled(false);
        add(title);
        closeButton.addActionListener(this);
        add(closeButton);
        setOpaque(true);

    }

    public void actionPerformed(ActionEvent e) {
        int i = tabbedPane.indexOfTabComponent(this);
        if (i != -1) {
            tabbedPane.remove(i);
        }
    }
}
    

