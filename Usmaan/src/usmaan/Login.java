package usmaan;

import common.JDBC;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.UIManager;

public class Login extends javax.swing.JFrame {

    public Login() {
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
        loading();
        txt_uname.grabFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txt_uname = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_pwd = new javax.swing.JPasswordField();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        lbl_text = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setText("      ");

        jLabel8.setBackground(new java.awt.Color(0, 0, 0));
        jLabel8.setFont(new java.awt.Font("Gisha", 0, 21)); // NOI18N
        jLabel8.setText("Inventory System");

        jLabel1.setBackground(new java.awt.Color(0, 0, 0));
        jLabel1.setFont(new java.awt.Font("Gisha", 0, 20)); // NOI18N
        jLabel1.setText("Usmaan Enterprices");

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("Gisha", 0, 12)); // NOI18N
        jLabel2.setText("<html>123/1,  Main Street , Kurunegala.\n</html>");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/Texture.png"))); // NOI18N
        jLabel5.setText("jLabel5");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel4)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 414, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 548, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel4)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(0, 0, 0)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jLabel6.setFont(new java.awt.Font("Gisha", 0, 11)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 0, 0));
        jLabel6.setText("<html><b><i>Developed  by Ushan Tennakoon </i></b></html>");
        jPanel4.add(jLabel6);

        getContentPane().add(jPanel4, java.awt.BorderLayout.PAGE_END);

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setMaximumSize(new java.awt.Dimension(32767, 50));
        jPanel7.setMinimumSize(new java.awt.Dimension(190, 80));
        jPanel7.setPreferredSize(new java.awt.Dimension(80, 50));
        jPanel7.setLayout(new java.awt.GridLayout(12, 0));

        jLabel11.setText("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        jPanel1.add(jLabel11);

        jPanel7.add(jPanel1);

        jLabel10.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        jLabel10.setText("Username : ");
        jPanel3.add(jLabel10);

        txt_uname.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        txt_uname.setPreferredSize(new java.awt.Dimension(180, 30));
        txt_uname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_unameActionPerformed(evt);
            }
        });
        txt_uname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_unameFocusGained(evt);
            }
        });
        txt_uname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_unameKeyPressed(evt);
            }
        });
        jPanel3.add(txt_uname);

        jLabel9.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        jLabel9.setText("           Password : ");
        jPanel3.add(jLabel9);

        txt_pwd.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        txt_pwd.setPreferredSize(new java.awt.Dimension(180, 30));
        txt_pwd.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_pwdFocusGained(evt);
            }
        });
        txt_pwd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_pwdKeyPressed(evt);
            }
        });
        jPanel3.add(txt_pwd);

        jPanel7.add(jPanel3);

        jButton1.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        jButton1.setText("Login");
        jButton1.setPreferredSize(new java.awt.Dimension(150, 30));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton1);

        jButton2.setFont(new java.awt.Font("Gisha", 1, 12)); // NOI18N
        jButton2.setText("Exit");
        jButton2.setPreferredSize(new java.awt.Dimension(150, 30));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel5.add(jButton2);

        jPanel7.add(jPanel5);

        jLabel12.setText("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
        jPanel6.add(jLabel12);

        jPanel7.add(jPanel6);

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lbl_text.setFont(new java.awt.Font("Gisha", 1, 18)); // NOI18N
        lbl_text.setForeground(new java.awt.Color(204, 0, 51));
        lbl_text.setText("Enter Username and Password");
        lbl_text.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jPanel8.add(lbl_text);

        jPanel7.add(jPanel8);

        getContentPane().add(jPanel7, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(1189, 816));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {

            if (txt_uname.getText().isEmpty()) {
                lbl_text.setText("Enter Username..!");
                txt_uname.grabFocus();
            } else if (txt_pwd.equals("")) {
                lbl_text.setText("Enter Password..!");
                txt_pwd.grabFocus();
            } else {
                continueLogin();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_unameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_unameFocusGained
        if (Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
            lbl_text.setText("Caution: Caps Lock is on!");
        } else {
            lbl_text.setText("Enter Username and Password");
        }
    }//GEN-LAST:event_txt_unameFocusGained

    private void txt_unameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_unameKeyPressed
        if (Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
            lbl_text.setText("Caution: Caps Lock is on!");
        } else {
            lbl_text.setText("Enter Username and Password");
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txt_pwd.grabFocus();
        } else if (txt_uname.getText().startsWith("Username")) {
            lbl_text.setText("Enter Username and Password");
        }
    }//GEN-LAST:event_txt_unameKeyPressed

    private void txt_pwdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_pwdFocusGained
        if (Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK)) {
            lbl_text.setText("Caution: Caps Lock is on!");
        } else {
            lbl_text.setText("Enter Username and Password");
        }
    }//GEN-LAST:event_txt_pwdFocusGained

    private void txt_unameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_unameActionPerformed
    }//GEN-LAST:event_txt_unameActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txt_pwdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_pwdKeyPressed
        if (evt.getKeyCode() == 10) {
            String s = new String(txt_pwd.getPassword());
            if (!s.isEmpty()) {
                jButton1.doClick();
            }
        }
    }//GEN-LAST:event_txt_pwdKeyPressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JLabel lbl_text;
    private javax.swing.JPasswordField txt_pwd;
    private javax.swing.JTextField txt_uname;
    // End of variables declaration//GEN-END:variables

    private void loading() {
        try {
            Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
            this.setSize(dimension);
            this.setTitle("Usmaan Enterprises");
        } catch (Exception e) {
        }
    }

    private void continueLogin() {

        try {
            Connection connection = JDBC.usmaanEnterprises_con();
            String inputUname = txt_uname.getText();
            String inputPwd = String.valueOf(txt_pwd.getPassword());
            PreparedStatement ps = connection.prepareStatement("SELECT `username`,`password` FROM `login` WHERE username=?");
            ps.setString(1, inputUname);
            ResultSet rs = ps.executeQuery();
            if(rs.isBeforeFirst()){
                rs.first();
                if (inputUname.equals(rs.getString("username")) && inputPwd.equals(rs.getString("password"))) {
                   this.dispose();
                   new home().setVisible(true);
                   
                   
                } else {
                    lbl_text.setText("User name & Password not accepted. Please Try Again!");
                }
            }else{
             
                    lbl_text.setText("User name & Password not accepted. Please Try Again!");
                
            }
          
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
