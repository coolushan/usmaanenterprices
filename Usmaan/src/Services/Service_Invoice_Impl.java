/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Models.Product;
import common.JDBC;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Service_Invoice_Impl implements Service_Invoice {

    public List<Product> findbyName(String name, Connection con) throws Exception {

        String sql = "SELECT `pid`,`description`,`pname`,`quantity`,`reg_date`,`uom`,`brand`,`status_status_id`,unitprice FROM `product` WHERE pname LIKE '%" + name + "%' and status_status_id=1";
        PreparedStatement ps = con.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        List<Product> pList = new ArrayList<Product>();
        while (rs.next()) {
            Product product = new Product();
            product.setBrand(rs.getInt("brand"));
            product.setDescription(rs.getString("description"));
            product.setPid(rs.getInt("pid"));
            product.setPname(rs.getString("pname"));
            product.setQuantity(rs.getBigDecimal("quantity"));
            product.setReg_date(rs.getString("reg_date"));
            product.setUom(rs.getString("uom"));
            product.setUnitprice(rs.getBigDecimal("unitprice"));
            pList.add(product);

        }
        return pList;

    }

    @Override
    public List<Product> findbyAll(Connection con) throws Exception {
        String sql = "SELECT `pid`,`description`,`pname`,`quantity`,`reg_date`,`uom`,`brand`,`status_status_id`,unitprice FROM `product` WHERE  status_status_id=1";
        PreparedStatement ps = con.prepareStatement(sql);

        ResultSet rs = ps.executeQuery();
        List<Product> pList = new ArrayList<Product>();
        while (rs.next()) {
            Product product = new Product();
            product.setBrand(rs.getInt("brand"));
            product.setDescription(rs.getString("description"));
            product.setPid(rs.getInt("pid"));
            product.setPname(rs.getString("pname"));
            product.setQuantity(rs.getBigDecimal("quantity"));
            product.setReg_date(rs.getString("reg_date"));
            product.setUom(rs.getString("uom"));
            product.setUnitprice(rs.getBigDecimal("unitprice"));
            pList.add(product);

        }
        return pList;
    }

    @Override
    public Product findbyItemCode(Connection con, int ItemCode) throws Exception {
        String sql = "SELECT `pid`,`description`,`pname`,`quantity`,`reg_date`,`uom`,`brand`,`status_status_id`,unitprice FROM `product` WHERE  status_status_id=1 and pid=?";
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setInt(1, ItemCode);
        ResultSet rs = ps.executeQuery();

        if (rs.first()) {
            Product product = new Product();
            product.setBrand(rs.getInt("brand"));
            product.setDescription(rs.getString("description"));
            product.setPid(rs.getInt("pid"));
            product.setPname(rs.getString("pname"));
            product.setQuantity(rs.getBigDecimal("quantity"));
            product.setReg_date(rs.getString("reg_date"));
            product.setUom(rs.getString("uom"));
            product.setUnitprice(rs.getBigDecimal("unitprice"));
            return product;

        } else {
          
            return null;
        }

    }

}
