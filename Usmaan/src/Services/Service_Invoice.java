/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Models.Product;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author User
 */
public interface Service_Invoice {
    
      List<Product> findbyName(String name,Connection Comn) throws Exception;
      List<Product> findbyAll( Connection Comn) throws Exception;

    public Product findbyItemCode(Connection con, int ItemCode) throws Exception;
      
      
}
