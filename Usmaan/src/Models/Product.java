/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.math.BigDecimal;

/**
 *
 * @author User
 */
public class Product {

    private Integer pid;
    private String description;
    private String pname;
    private BigDecimal quantity;
    private String reg_date;
    private String uom;
    private Integer brand;
    private String brandname;
     private BigDecimal unitprice;
    private Integer status_status_id;
    private String status_status_name;

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    
    
    
    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public Integer getBrand() {
        return brand;
    }

    public void setBrand(Integer brand) {
        this.brand = brand;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public Integer getStatus_status_id() {
        return status_status_id;
    }

    public void setStatus_status_id(Integer status_status_id) {
        this.status_status_id = status_status_id;
    }

    public String getStatus_status_name() {
        return status_status_name;
    }

    public void setStatus_status_name(String status_status_name) {
        this.status_status_name = status_status_name;
    }

    @Override
    public String toString() {
        return pname;
    }
    
    
}
